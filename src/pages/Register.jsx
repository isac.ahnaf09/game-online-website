import React, { useRef, useState } from 'react';
import { Form, FormGroup, Label, Button, Input, FormText} from 'reactstrap'
import Styles from '../assets/css/login.module.css'
import { useNavigate } from 'react-router-dom'
//firebase
import { createUserWithEmailAndPassword  } from "firebase/auth";
import { auth, db } from "../services/firebase";
import { getStorage, ref, uploadBytes } from "firebase/storage";
import { setDoc, doc } from 'firebase/firestore'

//sweetalert
import swal from 'sweetalert2/dist/sweetalert2';


const Register = () => {
    const [user, setUser] = useState({
        email : '',
        password : '',
        fullname : '',
        image : null,
        imageURL : '',
        isSuccess : true
    }) 
    const fileRef = useRef()
    const navigate = useNavigate()
    

    const uploadImage = (e) => {
        e.preventDefault()
        

        const file = fileRef.current.files[0]
        const reader = new FileReader()

        if(file){
            reader.addEventListener('load', () =>{
                setUser({ ...user, image : reader.result })
            }, false)
    
            if(file.type.includes('image/')) reader.readAsDataURL(file)
            
        }else{
            //handle if picture has been removed
            setUser({image:null})
        }
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUser((prevValue) => {
            const updatedValue = { ...prevValue, [name]: value };
            return updatedValue;
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault()

        try {
            /* UPLOAD PROFILE IMAGE */
            let file = fileRef.current.files[0]

            if(file === undefined) file = { name : "default_profile_icon.png" }

            const storage = getStorage();
            const storageRef = ref(storage, `images/${file.name}`);
            
            //alert progres registering
            swal.fire({
                title : 'Registering...',
                text : 'wait for a second..',
                timer : 5000,
                allowEscapeKey: false,
                allowOutsideClick: false,
                showConfirmButton: false
            })
        
            /* UPLOAD IMAGE TO STORAGE */
            await uploadBytes(storageRef, file)
                .then(() => {
                    //set image url
                    setUser({ ...user, imageURL : `images/${file.name}` })
                }).catch((error) => {
                    const errorMessage = error.message;
                    setUser({ ...user, isSuccess : false })
                    
                    return new swal({
                        icon: 'error',
                        title: 'Oops...',
                        text: `something went wrong !! ${errorMessage}`,
                    })
                })
            
            /* CREATE NEW USER */
            await createUserWithEmailAndPassword(auth, user.email, user.password)
                .then(async (userCredential) => {
                    //signed in
                    const userData = userCredential.user;
                    
                    //set token to local storage
                    localStorage.setItem('accessToken', user.accessToken)

                    //store data to firestore
                    await setDoc(doc(db, "users", userData.uid), {
                        email : user.email,
                        fullname : user.fullname,
                        photoURL : user.imageURL
                    });
    
                    
                }).catch((error) => {
                    const errorMessage = error.message;
                    
                    setUser({ ...user, isSuccess : false })
                    return new swal({
                        icon: 'error',
                        title: 'Oops...',
                        text: `something went wrong !! ${errorMessage}`,
                    })
                })
                
                

                if(user.isSuccess === true) {
                    new swal({ 
                        title: 'Successfully Registered!',
                        icon: 'success',
                        timer: 2000,
                        showConfirmButton: false
                    })
                }
                
                return navigate('/login')
        } catch (error) {
            const errorMessage = error.message;
            
            new swal({
                icon: 'error',
                title: 'catch',
                text: `something went wrong !! ${errorMessage}`,
            })
        }
    } 

    return (
        <div className={`Login-component`}>
            <div className={`${Styles.background_register}`}>
            <h5 className='text-center text-white '>Register</h5>
            <Form onSubmit={handleSubmit}>
                <div className="d-flex justify-content-center">
                    {user.image === null ? 
                        <img src="./images/default_profile_icon.png" alt="" style={{
                            width : "150px",
                            height : "150px",
                            borderRadius : "100px",
                            marginBottom: "20px",
                            marginTop : "20px"
                        }}/>
                        :
                        <img src={user.image} alt="" style={{
                            width : "150px",
                            height : "150px",
                            borderRadius : "100px",
                            marginBottom: "20px",
                            marginTop : "20px" 
                        }}/>
                    }
                </div>
                <FormGroup className={`${Styles.center} text-white`}>
                    <Label for="email">Email</Label>
                    <Input
                        id="email"
                        name="email"
                        placeholder="Email"
                        type="email"
                        required
                        value={user.email}
                        onChange={handleChange}
                    />
                </FormGroup>
                {' '}
                <FormGroup className={`${Styles.center} text-white`}>
                    <Label for="password">Password</Label>
                    <Input
                        id="password"
                        name="password"
                        placeholder="Password"
                        type="password"
                        required
                        value={user.password}
                        onChange={handleChange}
                    />
                    </FormGroup>
                {' '}
                <FormGroup className={`${Styles.center} text-white`}>
                    <Label for="fullname">Fullname</Label>
                    <Input
                        id="fullname"
                        name="fullname"
                        placeholder="Fullname"
                        type="text"
                        value={user.fullname}
                        onChange={handleChange}
                    />
                </FormGroup>
                {' '}
                <FormGroup className={`${Styles.center} text-white`}>
                    <Label for="profilePicture">Profile Picture</Label>
                    <Input
                        id="pictureProfile"
                        name="file"
                        type="file"
                        bsSize="lg"
                        accept="image/*"
                        innerRef={fileRef}
                        onChange={uploadImage}
                    />
                    <FormText>
                    upload your profile image here. *you can skip this and use default image
                    </FormText>
                </FormGroup>
                {' '}
                <div className={`${Styles.center}`}>
                <Button className={`${Styles.button} text-center mb-4`}>Register</Button>
                <a className='text-white' href='/login'>Already have an account? Login here</a>
                </div>
            </Form> 
            </div>
        </div>
    )
}

export default Register;
