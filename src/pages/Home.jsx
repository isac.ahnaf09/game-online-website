import React from "react";
import "../assets/css/content.css";
import NewsBanner from "../components/NewsBanner";
import Trending from "../components/Trending";
import { Container, Row, Col } from "reactstrap";
import Sidebar from "../components/Sidebar";
import withAuth from "../components/withAuth";


function Home() {

  return (
    <>
      <Container fluid>
        <Row>
          <Col xs="2">
            <Sidebar />
          </Col>
          <Col xs="10">
          <div>
            <h1 className="ms-3 mb-4">News Today</h1>
            <NewsBanner />
            <h1 className="ms-3 mb-4 mt-5">Trending Games</h1>
            <Trending />
          </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default withAuth(Home)