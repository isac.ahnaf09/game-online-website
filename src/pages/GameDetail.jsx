import React, { useState } from "react";
import "../assets/css/GameDetail.css";
import withAuth from "../components/withAuth";


function RockPaperScissor() {
  const [computerPick, setComputerPick] = useState("");
  const [playerPick, setPlayerPick] = useState("");
  const [result, setResult] = useState("");
  const options = ["Rock", "Paper", "Scissor"];

  function handlePlayerPick(pick) {
    setPlayerPick(pick);
    const computer = options[Math.floor(Math.random() * 3)];
    setComputerPick(computer);

    if (pick === computer) {
      setResult("Draw");
    } else if ((pick === "Rock" && computer === "Scissor") || (pick === "Paper" && computer === "Rock") || (pick === "Scissor" && computer === "Paper")) {
      setResult("You Win!");
    } else {
      setResult("Computer Wins!");
    }
  }

  function renderResult() {
    if (result === "Draw") {
      return (
        <div className="replace result" style={{ background: "#035B0C" }}>
          DRAW
        </div>
      );
    } else if (result === "You Win!") {
      return (
        <div className="replace result" style={{ background: "#4C9654" }}>
          PLAYER 1 <br /> WIN
        </div>
      );
    } else if (result === "Computer Wins!") {
      return (
        <div className="replace result" style={{ background: "#4C9654" }}>
          COM <br /> WIN
        </div>
      );
    }
  }

  return (
    <>
      <div className="container-fluid header d-flex">
        <div className="back-container ms-3 me-2">
          <a href="/home" className="back">
            &lt;
          </a>
        </div>
        <div className="logo-container">
          <img className="ms-4 me-2" src="images/logo.png" alt="logo" />
        </div>
        <div className="title-container">
          <h1 className="title ms-3">ROCK PAPER SCISSORS</h1>
        </div>
      </div>
      <div className="container text-center main-games">
        <div className="row">
          <div className="col-4 d-flex left">Player 1</div>
          <div className="col-4"></div>
          <div className="col-4 d-flex right">
            <div>COM</div>
          </div>
        </div>
        <div className="row rock">
          <div className="col-4 d-flex left">
            <div id="player-rock" className="rectangle d-flex" onClick={() => handlePlayerPick("Rock")}>
              <img src="images/batu.png" className="rock-img" alt="batu" />
            </div>
          </div>
          <div className="col-4"></div>
          <div className="col-4 d-flex right">
            <div id="com-rock" className="rectangle d-flex">
              <img src="images/batu.png" alt="batu" className="rock-img" style={computerPick === "Rock" ? { background: "#C4C4C4" } : { background: "#9C835F" }} />
            </div>
          </div>
        </div>
        <div className="row paper">
          <div className="col-4 d-flex left">
            <div id="player-paper" className="rectangle d-flex" onClick={() => handlePlayerPick("Paper")}>
              <img src="images/kertas.png" alt="kertas" className="paper-img" />
            </div>
          </div>
          <div className="col-4 d-flex versus">
            <div><h1>{result}</h1> <br /></div>
            <div className="replace d-flex">VS</div>
          </div>
          <div className="col-4 d-flex right">
            <div id="com-paper" className="rectangle d-flex">
              <img src="images/kertas.png" alt="kertas" className="paper-img" style={computerPick === "Paper" ? { background: "#C4C4C4" } : { background: "#9C835F" }} />
            </div>
          </div>
        </div>
        <div className="row scissor">
          <div className="col-4 d-flex left">
            <div id="player-scissor" className="rectangle d-flex" onClick={() => handlePlayerPick("Scissor")}>
              <img src="images/gunting.png" alt="gunting" className="scissor-img" />
            </div>
          </div>
          <div className="col-4"></div>
          <div className="col-4 d-flex right">
            <div id="com-scissor" className="rectangle d-flex">
              <img src="images/gunting.png" alt="gunting" className="scissor-img" style={computerPick === "Scissor" ? { background: "#C4C4C4" } : { background: "#9C835F" }} />
            </div>
          </div>
        </div>
        <div className="row result">
          <div className="col-12">
          </div>
        </div>
      </div>
    </>
  );
}

export default withAuth(RockPaperScissor)
