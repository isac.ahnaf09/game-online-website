import GameListStyle from '../assets/css/GameList.module.css';
import { Link } from 'react-router-dom';

export default function GameList(){
    return (
        <div className={GameListStyle.page}>
            <h5 className="pt-4 ps-5 ms-2">Recommended for you</h5>
            <div className="d-flex mx-4 mb-4 mt-2 justify-content-center">
                <Link 
                    to="/gameplay">
                        <img src="./images/game.jpg" height="205px" width="320px" className="rounded" alt="game"></img>
                    </Link>
                <div className="d-flex flex-column mx-2">
                    <Link 
                    to="/gameplay">
                        <img src="./images/blox.png" height="100px" width="155px" className="rounded" alt="game"></img>
                    </Link>
                    <Link 
                    to="/gameplay">
                        <img src="./images/capybara.png" height="100px" width="155px" className="rounded mt-1" alt="game"></img>
                    </Link>
                </div>
                <div className="d-flex flex-column me-2">
                    <Link 
                    to="/gameplay"> 
                        <img src="./images/smashcarts.png" height="100px" width="155px" className="rounded" alt="game"></img>
                    </Link>
                    
                    <Link 
                    to="/gameplay">
                        <img src="./images/buildnow.png" height="100px" width="155px" className="rounded mt-1" alt="game"></img>
                    </Link>
                </div>
                <Link 
                    to="/gameplay">
                    <img src="./images/cubes.png" height="205px" width="320px" className="rounded me-2" alt="game"></img>
                </Link>
                <div className="d-flex flex-column me-2">
                    <Link 
                    to="/game">
                        <img src="./images/flips.png" height="100px" width="155px" className="rounded" alt="game"></img>
                    </Link>
                    <Link 
                    to="/game">
                        <img src="./images/helixjump.png" height="100px" width="155px" className="rounded mt-1" alt="game"></img>
                    </Link>
                </div>
            </div>

            <h5 className="px-4 ps-5 ms-3">Featured games</h5>
            <div className="d-flex mx-4 mb-4 mt-2 justify-content-center">
                <Link 
                    to="/game">
                    <img src="./images/taptapshots.png" height="100px" width="155px" className="rounded mx-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/highwayracer.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/braindom.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/funnyblade.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/stabfish.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/pitstop.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/battledudes.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
            </div>

            <h5 className="px-4 ps-5 ms-3">New games</h5>
            <div className="d-flex mx-4 mb-4 mt-2 justify-content-center">
                <Link 
                    to="/game">
                    <img src="./images/thisorthat.png" height="100px" width="155px" className="rounded mx-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/pushthem.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/teachersimulation.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/evohero.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/endlesssiege.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/snipermaster.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/sniper3d.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
            </div>

            <h5 className="px-4 ps-5 ms-3">Casual games</h5>
            <div className="d-flex mx-4 pb-4 mt-2 justify-content-center">
                <Link 
                    to="/game">
                    <img src="./images/jetrush.png" height="100px" width="155px" className="rounded mx-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/stackcolors.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/farmland.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/driftboss.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/cardrawing.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/chess.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
                <Link 
                    to="/game">
                    <img src="./images/masterchess.png" height="100px" width="155px" className="rounded me-2" alt="game"></img>
                </Link>
            </div>
        </div>
        
    )
}