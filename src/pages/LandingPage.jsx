import Navbar from "../components/Navbar";
import Carousel from "../components/Carousel";
import "../assets/css/LandingPage.css";
import React from 'react'

export default function LandingPage() {

  return (
    <>
      <section id="main-screen">
        <Navbar />
        <div>
          <div className="text-center text-white highlight-text">
            <h1 className="text-uppercase fw-bold mb-4">Play Traditional Game</h1>
            <p className="mb-4">Experience new traditional game play</p>
            <button type="button" className="btn btn-warning text-uppercase fw-bold button">
             <a style={{textDecoration:"none"}} className="text-white"href="/list-game">Play Now</a> 
            </button>
          </div>
          <div className="text-uppercase text-white" id="the-story">
            <p>The Story</p>
            <img src="./images/scroll-down.svg" alt="scroll-down" />
          </div>
        </div>
      </section>

      <section id="about">
        <div className="container">
          <div className="row">
            <div className="col-3 text-white text-uppercase">
              <p>What's so spesial?</p>
              <h2>The Games</h2>
            </div>
            <div className="col-9">
              <Carousel />
            </div>
          </div>
        </div>
      </section>

      <section id="features">
        <div className="container text-white">
          <div className="row">
            <div className="col-md-4 offset-md-8">
              <h4>What's so special?</h4>
            </div>
            <div className="col-md-4 offset-md-8">
              <h2>Features</h2>
            </div>
            <br /> <br /> <br /> <br />
            <div className="col-md-4 offset-md-8">
              <h4>Traditional Games</h4>
            </div>
            <div className="col-md-4 offset-md-8 mb-3">
              <p>if you miss your child hood, we provide many traditional games here</p>{" "}
            </div>
            <div className="col-md-4 offset-md-8 mb-3">
              <h4>GAME SUIT</h4>
            </div>
            <div className="col-md-4 offset-md-8 mb-3">
              <h4>TBD</h4>
            </div>
          </div>
        </div>
      </section>

      <section id="requirements">
        <p className="text-center text-white">Can my computer run the game?</p>
        <div className="container">
          <div className="row text-white">
            <div className="col-md-6">
              <h2>System requirements</h2>
              <div className="col-md-12">
                <div className="container">
                  <div className="row row-cols-2 text-white">
                    <div className="col border 1px">
                      OS:
                      <p>Windows 7 64 bit only (No OSX support at this time)</p>
                    </div>
                    <div className="col border 1px">
                      PROCESSOR:
                      <p>Intel Core 2 Duo @2.4 GHZ or AMD Athlon X2 @2.8 GHZ</p>
                    </div>
                    <div className="col border 1px">
                      Memory:
                      <p>4 GB RAM</p>
                    </div>
                    <div className="col border 1px">
                      Storage:
                      <p>8 GB available space</p>
                    </div>
                  </div>
                  <div className="row row-cols-1 col-md-13 text-white">
                    <div className="col border 1px">
                      GRAPHICS:
                      <p>
                        NVIDIA GeForce GTX 660 2GB or
                        <br />
                        AMD Radeon HD 7850 2GB DirectX11 (Shader Model 5)
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="top-scores">
        <div className="col">
          <div className="row">
            <div className="col-lg-4 offset-lg-1 top-score-text">
              <h2 className="mb-4">TOP SCORE </h2>
              <p className="mb-4">This top score from various games provided on this platform</p>
              <button type="button" className="btn btn-warning fw-bold">
                see more
              </button>
            </div>
            <div className="col">
              <div className="row">
                <div className="col-lg-9 offset-lg-4 mb-4">
                  <img src="./images/quote-1.png" alt="quote1" />
                </div>
                <div className="col-lg-9 offset-lg-2 mb-4">
                  <img src="./images/quote-2.png" alt="quote2" />
                </div>
                <div className="col-lg-9 offset-lg-4 mb-4">
                  <img src="./images/quote-3.png" alt="quote3" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="newsletter">
        <div className="container col">
          <div className="row">
            <div className="col-lg-4 offset-lg-1 newsleteter-image">
              <img src="./images/skull.png" alt="skull" />
            </div>
            <div className="col" id="newsletter-text">
              <div className="row text-white">
                <div className="col-lg-9 offset-lg-3 mb-4">
                  <p>Want to stay in touch?</p>
                  <h2 className="text-uppercase">NEWSLETTER SUBSCRIBE</h2>
                  <p className="mb-5">
                    In order to start receiving our news, all you have to do is enter your email address. Everything else will be taken care of by us. We will send you emails containing information about game. We don’t spam.
                  </p>
                  <form>
                    <input type="email" placeholder="Your Email Address" />
                    <button type="submit" className="btn btn-warning main-btn button">
                      Subscribe now
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <footer className="text-white" id="footer">
        <ul className="nav justify-content-end">
          <li className="nav-item">
            <a className="nav-link active ms-3" aria-current="page" href="#main-screen">
              Main
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#about">
              About
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#features">
              Game Feature
            </a>
          </li>
          <li className="nav-item">
          <a className="nav-link ms-3" href="#requirements">
            System requirements</a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#top-scores">
              Quotes
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#">
              <img src="./images/facebook.svg" alt="facebook" />
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#">
              <img src="./images/twitter.svg" alt="twitter" />
            </a>
          </li>
          <li className="nav-item ms-3">
            <a className="nav-link disabled">
              <img src="./images/vector.svg" alt="youtube" />
            </a>
          </li>
          <li className="nav-item ms-3">
            <a className="nav-link disabled">
              <img src="./images/twitch.svg" alt="twitch" />
            </a>
          </li>
        </ul>
        <div id="copyright">
          <nav className="navbar navbar-expand-lg navbar-dark">
            <div className="container">
              <a className="navbar-nav mb-2 mb-lg-0" id="copyright-2018">
                © 2018 Your Games, Inc. All Rights Reserved
              </a>
              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0"></ul>
                <ul className="navbar-nav d-flex">
                  <li className="nav-item">
                    <a className="nav-link active" aria-current="page" href="#">
                      privacy policy
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      terms of service
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      code of conduct
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </footer>
    </>
  );
}
