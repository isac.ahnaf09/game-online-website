import React, { useState,useEffect } from 'react';
import { Alert,Form, FormGroup, Label, Button, Input } from 'reactstrap';
import Styles from '../assets/css/login.module.css';
import { auth } from '../services/firebase';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { useNavigate } from 'react-router-dom';

const Login = () => {
  const [user, setUser] = useState({
    email: '',
    password: '',
  });
  const navigate = useNavigate();
  const [passwordError, setPasswordError] = useState('');

  const signIn = (e) => {
    e.preventDefault();

    signInWithEmailAndPassword(auth, user.email, user.password)
      .then((userCredential) => {
        console.log(userCredential);
        //store token in local storage
        userCredential.user.getIdToken().then((token)=>{
          localStorage.setItem('token',token);
          navigate('/home');
        })
      })
      .catch((error) => {
        console.log(error);
        setPasswordError(
          <Alert
            color="danger"
            className='text-center'
            style={{ width: '70%', margin: 'auto', padding: '5px' }}
          >
            Wrong Password
          </Alert>
        );
        setTimeout(() => {
          setPasswordError('');
        }, 5000);
      });
  };

  useEffect(()=>{
    const token = localStorage.getItem('token')
    if(token){
      navigate('/home')
    }
  })

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser((prevValue) => {
      const updatedValue = { ...prevValue, [name]: value };
      return updatedValue;
    });
  };
  
  return (
    <div className={`Login-component`}>
      <div className={`${Styles.background}`}>
        <h5 className='text-center text-white '>Account Login</h5>
        <div>{passwordError}</div>
        <Form onSubmit={signIn}>
          <FormGroup className={`${Styles.center} text-white`}>
            <Label for='exampleEmail'>Email</Label>
            <Input
              id='username'
              name='email'
              placeholder='Email'
              type='text'
              value={user.email}
              onChange={handleChange}
            />
          </FormGroup>
          <FormGroup className={`${Styles.center} text-white`}>
            <Label for='examplePassword'>Password</Label>
            <Input
              id='examplePassword'
              name='password'
              value={user.password}
              placeholder='Password'
              type='password'
              onChange={handleChange}
            />
          </FormGroup>
          <div className={`${Styles.center}`}>
            <Button className={`${Styles.button} text-center mb-4`}>Login</Button>
            <a className='text-white d-flex mb-2' href='/register'>Forget password?</a>
            <a className='text-white' href='/register'>Don't have an account? Register here</a>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Login;
