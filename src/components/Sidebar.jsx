import React,{useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import '../assets/css/sidebar.css';
import { FaStar, FaSignOutAlt } from 'react-icons/fa';
import { signOut,onAuthStateChanged } from "firebase/auth";
import {auth} from '../services/firebase'
import { Navigate } from 'react-router';

function Sidebar() {

  const [email,setEmail]= useState('')

  useEffect(()=>{
    const whoAmI = onAuthStateChanged(auth, (user)=>{
      if(user){
        setEmail(user.email)
      }else{
        <Navigate to= '/login' />
      }
    });
    return whoAmI
  })

  function handleSignOut() {
    signOut(auth).then(() => {
      // Sign-out successful
      // Remove token from local storage
      localStorage.removeItem('token');
      <Navigate to="/login" />
    }).catch((error) => {
      // An error happened.
      alert(error)
    });
  }

  
  
  return (
    <nav className="sidebar">
      <ul className="sidebar__list">
        <li className='text-center'>
          <img src='./images/profile-picture.jpg' className='rounded-circle profile-picture mb-3'/>
        </li>
        <li className='text-center profile'>
          User Name
        </li>
        <li className='text-center profile mb-5'>
          {email}
        </li>
        <li>
            <Link to="/" className="sidebar__link"> <FaStar className='star-icon'/> Home</Link>
        </li>
        <li>
          <Link to="/list-game" className="sidebar__link"> <FaStar className='star-icon'/> List Game</Link>
        </li>
        <li>
          <Link to="/listuser" className="sidebar__link"> <FaStar className='star-icon'/> List Profile</Link>
        </li>
        <li>
          <Link to="/leaderboard" className="sidebar__link"> <FaStar className='star-icon'/> Leaderboard</Link>
        </li>
        <li>
          <Link  className="logout" onClick={handleSignOut}> <FaSignOutAlt className='star-icon'/> Logout</Link>
        </li>
      </ul>
    </nav>
  );
}

export default Sidebar;
