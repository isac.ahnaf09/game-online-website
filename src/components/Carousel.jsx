import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';

const items = [
  {
    src: './images/rockpaperstrategy-1600.jpg',
    altText: 'Rock Paper Scissor',
    caption: 'Play Rock Paper Scissor Online Games',
    header: 'Rock Paper Scissor'
  },
  {
    src: './images/block2024io.jpeg',
    altText: 'Cubes 2024.io',
    caption: 'Play Cubes 2024.io Online Games',
    header: 'Cubes 2024.io'
  },
  {
    src: './images/helix-jump-cover.avif',
    altText: 'Helix Jump',
    caption: 'Play Helix Jump Online Games',
    header: 'Helix Jump'
  }
];

const Example = () => <UncontrolledCarousel items={items} />;

export default Example;