import React from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../services/firebase'; // import firebase auth instance
import { Navigate } from 'react-router-dom';

const withAuth = (Component) => {
  const AuthComponent = (props) => {
    const [user, loading] = useAuthState(auth);

    if (loading) {
      return <div>Loading...</div>;
    }

    if (!user) {
      return <Navigate to="/login" />;
    }

    return <Component {...props} />;
  };

  return AuthComponent;
};

export default withAuth;
