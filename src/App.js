import React from 'react'
import './App.css';
import '@sweetalert2/theme-dark';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login'
import Register from './pages/Register'
import GameList from './pages/GameList';
import GameDetail from './pages/GameDetail';
import NotFound from './pages/NotFound';
import ListUser from './pages/ListUser';
import LandingPage from "./pages/LandingPage";

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<LandingPage />} />
        <Route exact path="/login" element={<Login/>}/>
        <Route exact path="/register" element={<Register/>}/>
        <Route exact path="/home"  element={<Home/>}/>
        <Route exact path="/list-game" element={<GameList/>}/>
        <Route exact path="/listuser" element={<ListUser/>}/>
\        <Route path="*" element={<NotFound />} />
        <Route exact path="/gameplay" element={<GameDetail/>}/>
      </Routes>
    </Router>
  )
}

export default App;
